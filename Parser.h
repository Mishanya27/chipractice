#pragma once
#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <cstddef>
#include <fstream>
#include <thread>
#include <mutex>
#include <chrono>

//typedef std::vector<std::wstring> parser_result;

enum Result {
	Success,
	Error
};

class CParser
{
public:
	CParser(size_t lines, size_t columns);
	~CParser();
	virtual void CParser::parseFromFile(std::wstring file);
	virtual Result get_result();

private:
	//parser_result m_name, m_year, m_lang, m_math, m_phys;
	size_t m_lines, m_columns; // ���-�� ����� � �������� ����� ��������������
	std::wstring **v; // ����� ����� ������ ������������ ����
	std::thread *t;
	//bool flag_error = false;
	virtual void parse(std::wstring file);
};


/*-------------------------------------------------------------------------------------------------------------------*/

#ifdef ___NOTHING___

#pragma once
#include <vector>
#include <cstddef>

typedef std::vector<std::wstring> parser_result;

enum Result {
    Success,
    Error
};

class CParser
{
public:
    CParser();
    ~CParser();
    /**
     *
     * @param pData - text data
     * @param size - size of data
     * @param parsed - size of the processed data
     * @return
     */
    virtual bool parse(wchar_t *pData, size_t size, size_t &parsed);

    virtual bool get(parser_result &result); // success -> true 

    virtual Result get_state();

    virtual Result get_result(); // block main thread
};

#endif