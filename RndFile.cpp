#include "RndFile.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>



CRndFile::CRndFile(char *fname)
{
	filename = fname;
}


CRndFile::~CRndFile()
{
	delete filename;
}

void CRndFile::FillFile()
{
	std::ofstream myfile(filename);

	int lang, math, phis, key, year;
	std::string Names[10] = { "Kovalevskiy",
		"Myrnenko",
		"Slabun",
		"Suslovets",
		"Malshakov",
		"Pivovar",
		"Sergienko",
		"Galetinko",
		"Kareev",
		"Voloshina"
	};

	for (size_t i = 0; i < 1000000; i++)
	{
		lang = rand() % 101 + 100;
		math = rand() % 101 + 100;
		phis = rand() % 101 + 100;
		key = rand() % 10;
		year = rand() % 11 + 2010;
		myfile << Names[key] << ';' << year << ';' << lang << ';' << math << ';' << phis << '\n';
	}
	std::cout << "Success";

	_getch();
}
