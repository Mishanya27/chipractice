#include "Parser.h"


CParser::CParser(size_t lines, size_t columns)
{
	m_lines = lines, m_columns = columns;
	v = new std::wstring*[m_lines]; //�������� ������ ��� ���������� �����
	for (size_t i = 0; i<m_lines; i++)
		v[i] = new std::wstring[m_columns]; //�������� ������ ��� ���������� ��������
}

CParser::~CParser()
{
	for (size_t i = 0; i < m_lines; i++)
	{
		delete[] v[i];
	}
}

void CParser::parse(std::wstring file)
{
	std::ifstream myfile(file);
	std::wstring temp;

	int ch = 0;  //��� ����������, � ������� ����� ��������� �������
	size_t lin = 0, col = 0;
	
	//������� ����� � ���������������
	while ((ch = myfile.get()) != EOF)
	{
		if (wchar_t(ch) != '\n') //��������� ������ �� ����� � ����� ��������� ��� �� ������� �������� ������
		{
			if (wchar_t(ch) == ';')
			{
				v[lin][col] = temp;
				temp.clear();
				col++;
			}
			else
				temp += wchar_t(ch);
		}
		else
		{
			v[lin][col] = temp;
			lin++;
			col = 0;
			temp.clear(); //������� ������
		}
	}
	myfile.close(); //��������� ����

	/*std::cout << "File has been read successfully" << std::endl;
	_getch();*/
}

void CParser::parseFromFile(std::wstring file) 
{
	// ���� ����� ������������� ������ �����
	this->t = new std::thread(&CParser::parse, this, file);
}


Result CParser::get_result()
{
	t->join();

	for (size_t i = 0; i < m_lines; i++)
	{
		for (size_t j = 0; j < m_columns; j++)
		{
			if (v[i][j].empty())
			{
				return Error; //flag_error = true;
			}
			else
			{
				return Success; //flag_error = false;
			}
		}
	}
}
