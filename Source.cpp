#include "RndFile.h"
#include "Parser.h"

std::mutex mtx;

int Menu()
{
	system("cls");
	
	std::cout << "Read file - 1" << std::endl;
	std::cout << "Fill file - 2" << std::endl;
	std::cout << "-------------" << std::endl;
	std::cout << "Exit      - 0" << std::endl;

	return _getch();
}

void ReadFile()
{
	mtx.lock();
	CParser prs(1000000, 5);
	prs.parseFromFile(L"Task.csv");
	//prs.Check();
	if (prs.get_result())
	{
		std::cout << "Some error" << std::endl;
		//_getch();
	}
	else
	{
		std::cout << "File has been read successfully" << std::endl;
		//_getch();
	}
	
	mtx.unlock();

	//std::ifstream myfile("Task.csv");
	//std::wstring temp;
	//std::wstring **v = new std::wstring*[1000000]; //�������� ������ ��� ���������� �����
	//for (size_t i = 0; i<1000000; i++)
	//	v[i] = new std::wstring[5]; //�������� ������ ��� ���������� �������� � ������

	//int ch = 0;  //��� ����������, � ������� ����� ��������� �������
	//size_t i = 0, j = 0;

	////������� ����� � ���������������
	//while ((ch = myfile.get()) != EOF) 
	//{   
	//	if (wchar_t(ch) != '\n') //��������� ������ �� ����� � ����� ��������� ��� �� ������� �������� ������
	//	{
	//		if (wchar_t(ch) == ';')
	//		{
	//			v[i][j] = temp;
	//			temp.clear();
	//			j++;
	//		}
	//		temp += wchar_t(ch);
	//	}
	//	else
	//	{
	//		v[i][j] = temp;
	//		i++;//v.push_back(s); //���� ������� ������ �������, �� ���������� ������ � ������
	//		j = 0;
	//		temp.clear(); //������� ������
	//	}
	//}    
	////v[i][j] = s; //���������� ��������� ������ � ������.
	//myfile.close(); //��������� ����

	//std::cout << "File has been read successfully" << std::endl;
	//_getch();

	/*for (size_t ii = 0; ii < 1000000; ii++)
	{
		for (size_t jj = 0; jj < 5; jj++)
		{
			std::wcout << v[ii][jj];
		}
		std::wcout << std::endl;
	}
	_getch();*/

	/*for (size_t i = 0; i < 1000000; i++)
	{
		delete [] v[i];
	}*/
}

//void Check()
//{
//	mtx.lock();
//	if (prs.get_result())
//	{
//		std::cout << "Some error" << std::endl;
//	}
//	else
//	{
//		std::cout << "File has been read successfully" << std::endl;
//	}
//	mtx.unlock();
//	_getch();
//}

int main()
{
	srand(time(0));
	CRndFile obj("Task.csv");
	std::thread thr_read(ReadFile);
	while (true)
	{
		switch (Menu())
		{
			case '0': exit(0); break;
			case '1': thr_read.join(); 
				/*thr_check.join();*/ break;
			case '2': obj.FillFile(); break;
			default: 
				std::cout << "Error" << std::endl;
				_getch();  break;
		}
	}
	
	return 0;
}